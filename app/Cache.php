<?php

namespace App;

use App\Interfaces\GetterCurrencyData;
use App\Interfaces\SetterCurrencyData;

class Cache implements GetterCurrencyData, SetterCurrencyData
{
    public function __construct()
    {
        // TODO: Реализовать настройку конфигурации
    }

    public function get(string $key) : array
    {
        // TODO: Implement get() method.
    }

    public function set(string $key, array $value): void
    {
        // TODO: Implement set() method.
    }

    public function __destruct()
    {
        // TODO: Terminate and close all transactions and sessions.
    }
}