<?php

namespace App\Interfaces;

interface GetterCurrencyData
{
    public function get(string $key) : array;
}