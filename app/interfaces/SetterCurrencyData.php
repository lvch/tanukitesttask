<?php

namespace App\Interfaces;

interface SetterCurrencyData
{
    public function set(string $key, array $value) : void;
}