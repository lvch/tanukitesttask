<?php

namespace App;

class Storage
{
    protected Cache $cache;
    protected Database $database;
    protected External $external;

    public function __construct()
    {
        $this->cache = new Cache();
        $this->database = new Database();
        $this->external = new External();
    }
}