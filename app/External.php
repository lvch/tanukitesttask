<?php

namespace App;

use App\Interfaces\GetterCurrencyData;

class External implements GetterCurrencyData
{

    public function __construct()
    {
        // TODO: Реализовать настройку конфигурации
    }

    /**
     * @param string $key
     * @return array
     * Тут может быть проблема с получением данных. Необходимо обработать ситуацию, когда
     * в качестве результата запроса придёт ошибка, либо сам запрос не сможет уйти
     * из-за проблем с сетью.
     * Поэтому при исключениях лучше возвращать пустой массив и производить запись в логе.
     */
    public function get(string $key) : array
    {
        // TODO: Implement get() method.
    }
}