<?php

namespace App;

class CurrencyStorage extends Storage
{
    /**
     * @param string $currency
     * @return array
     * Метод получения данных о курсе валют для конкретной валюты.
     * Лучше будет поставить обработчик исключений и в зависимости от типа исключения
     * выбрасивать обработанное исключение. В зависимости от того, как данный метод будет
     * использоваться (API или простое обращение к методу), необходимо реализовать
     * соответствующий возврат информации об ошибке.
     */
    public function getCurrency(string $currency) : array
    {
        if (empty($data = $this->cache->get($currency))) {
            if (empty($data = $this->database->get($currency))) {
                if (!empty($data = $this->external->get($currency))) {
                    $this->database->set($currency, $data);
                    $this->cache->set($currency, $data);
                }
            } else {
                $this->cache->set($currency, $data);
            }
        }

        return $data;
    }
}